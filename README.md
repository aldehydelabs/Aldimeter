# Aldimeter

Aldimeter offers you the possibility to check battery levels at a glance by means of a customizable overlay bar.  
You can check the remaining battery level during full screen mode such as gaming, videos etc.

<img src="screenshot.png" width="309" height="575">

## Download

Grab a build from the releases page and install it manually  
https://gitlab.com/aldehydelabs/Aldimeter/-/releases


Use F-Droid (coming soon ™)

## Credits

Aldimeter is a fork of [Android-OverlayBatteryBar](https://github.com/75py/Android-OverlayBatteryBar) by 75py  
Battery percentage icons from [Battery Indicator Pro](https://github.com/darshan-/Battery-Indicator-Pro) by Darshan Computing, LLC

## License

    Copyright 2017 75py
    Copyright 2021 LordAldehyde

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
