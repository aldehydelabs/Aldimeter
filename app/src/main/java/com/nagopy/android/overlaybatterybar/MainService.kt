package com.nagopy.android.overlaybatterybar

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.PixelFormat
import android.os.*
import android.support.v4.app.NotificationCompat
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowManager
import com.github.salomonbrys.kodein.android.KodeinService
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import timber.log.Timber
import java.util.*


class MainService : KodeinService() {

    val batteryBarDelegate: BatteryBarDelegate by with(this).instance()

    val notificationManager: NotificationManager by instance()

    var helperWnd: View? = null

    var fetchInfos: Timer? = null

    var fetchInfosRunnable: TimerTask? = null

    lateinit var n: NotificationCompat.Builder

    override fun onCreate() {
        super.onCreate()
        Timber.d("onCreate")

        inject(appKodein())

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }
        n = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
                .setContentTitle(getText(R.string.msg_running_title))
                .setContentText(getText(R.string.msg_running_text))
                .setSmallIcon(if (batteryBarDelegate.userSettings.isPercentageIcon()) R.drawable.l_outline else R.drawable.ic_stat_battery_unknown)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setContentIntent(PendingIntent.getActivity(this, 0,
                        Intent(this, MainActivity::class.java)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP),
                        PendingIntent.FLAG_UPDATE_CURRENT))

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            n = n.setCategory(Notification.CATEGORY_SERVICE)
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
        }

        // We are going to loose context, so fetch resources now
        var extras = Bundle()
        extras.putCharSequence("msg_running_text_stats", getText(R.string.msg_running_text_stats))
        extras.putCharSequence("msg_running_text", getText(R.string.msg_running_text))
        n.addExtras(extras)


        startForeground(1, n.build())

        batteryBarDelegate.batteryChangedCallback = { level: Int, scale: Int, isCharging: Boolean, temperature: Int, voltage: Int ->
            val iconLevel = if (isCharging && !batteryBarDelegate.userSettings.isPercentageIcon()) {
                level + 1000
            } else {
                level
            }

            if (batteryBarDelegate.userSettings.isAdvancedStats()) {
                n.extras.putInt("level", level)
                n.extras.putInt("scale", scale)
                n.extras.putFloat("temperature", temperature / 10.0f)

                if (voltage > 1000)
                    n.extras.putFloat("voltage", voltage / 1000f)
                else
                    n.extras.putFloat("voltage", voltage.toFloat())

                n.extras.putBoolean("charging", isCharging)
            }

            if (batteryBarDelegate.userSettings.isPercentageIcon())
                n.setSmallIcon(R.drawable.w000 + iconLevel)
            else
                n.setSmallIcon(R.drawable.ic_stat_battery_level, iconLevel)

            startForeground(1, n.build())
        }

        updateBatteryStats()
    }

    @TargetApi(Build.VERSION_CODES.O)
    fun createNotificationChannel() {
        val channel = NotificationChannel(NOTIFICATION_CHANNEL, getText(R.string.app_name), NotificationManager.IMPORTANCE_LOW).apply {
            enableLights(false)
            enableVibration(false)
            lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            importance = NotificationManager.IMPORTANCE_LOW
        }
        notificationManager.createNotificationChannel(channel)
    }

    private fun updateBatteryStats()
    {
        // This Task Runnable updates our battery information
        if (fetchInfosRunnable == null && batteryBarDelegate.userSettings.isAdvancedStats()) {
            fetchInfosRunnable = object : TimerTask() {
                private val mHandler: android.os.Handler = Handler(Looper.getMainLooper())
                override fun run() {
                    mHandler.post(Runnable {
                        var averageCurrent = 0
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            val batteryManager = batteryBarDelegate.context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
                            averageCurrent = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW) / 1000
                        }

                        n.setContentText(n.extras.getCharSequence("msg_running_text_stats")
                                .replace("\\%p\\%".toRegex(), n.extras.getInt("level", 0).toString())
                                .replace("\\%m\\%".toRegex(), averageCurrent.toString())
                                .replace("\\%t\\%".toRegex(), n.extras.getFloat("temperature", 0f).toString())
                                .replace("\\%v\\%".toRegex(), n.extras.getFloat("voltage", 0f).toString()))

                        startForeground(1, n.build())
                    })
                }
            }

            // Workaround weird exception when timer is already cancelled
            fetchInfos = Timer();
            fetchInfos!!.scheduleAtFixedRate(fetchInfosRunnable, 0, 5000)
        } else if (!batteryBarDelegate.userSettings.isAdvancedStats()) {
            // Remove task if user disables advanced Stats
            if (fetchInfos != null) {
                fetchInfos!!.cancel()
                fetchInfos!!.purge()
                fetchInfos = null
            }
            fetchInfosRunnable = null;
            n.setContentText(n.extras.getCharSequence("msg_running_text"))
            startForeground(1, n.build())
        }
    }

    // Hide batteryBar if a fullscreen app is playing
    private fun createHelperWnd() {
        // Avoid creating helper window twice, when it is already added
        if (helperWnd != null)
            return

        val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val p = WindowManager.LayoutParams()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            p.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            p.type = WindowManager.LayoutParams.TYPE_PHONE;
        }
        p.gravity = Gravity.RIGHT or Gravity.TOP
        p.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
        p.width = 1
        p.height = WindowManager.LayoutParams.MATCH_PARENT
        p.format = PixelFormat.TRANSPARENT
        helperWnd = View(this) //View helperWnd;
        wm.addView(helperWnd, p)

        var heightS = wm.defaultDisplay.height;
        var heightX = wm.defaultDisplay.width;

        var isFullScreen = false
        val vto: ViewTreeObserver = helperWnd!!.viewTreeObserver
        vto.addOnGlobalLayoutListener {
            isFullScreen = (heightS == helperWnd!!.height) || (heightX == helperWnd!!.height)

            if (isFullScreen)
                batteryBarDelegate.barView.view.visibility = View.INVISIBLE
            else
                batteryBarDelegate.barView.view.visibility = View.VISIBLE
        }
    }

    private fun destroyHelperWnd()
    {
        if (helperWnd != null) {
            val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
            wm.removeView(helperWnd)
            helperWnd = null
        }
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("onStartCommand")

        // Todo: This method is getting called excessively by the mainactivity's onCreate function

        if (batteryBarDelegate.isEnabled()) {
            batteryBarDelegate.stop()
            batteryBarDelegate.start()

            if (batteryBarDelegate.userSettings.isHideFullscreen())
                createHelperWnd()
            else
                destroyHelperWnd()

            updateBatteryStats()
        } else {
            destroyHelperWnd()
            stopSelf()
        }
        return START_STICKY
    }

    override fun onDestroy() {
        Timber.d("onDestroy")
        batteryBarDelegate.stop()
        destroyHelperWnd()
        if (fetchInfos != null) {
            fetchInfos!!.cancel()
            fetchInfos!!.purge()
            fetchInfos = null
        }
        fetchInfosRunnable = null
        super.onDestroy()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        batteryBarDelegate.stop()
        batteryBarDelegate.start()
    }

    override fun onBind(intent: Intent?): IBinder {
        throw RuntimeException("not implemented")
    }

    class Handler(val context: Context) {
        fun startService() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(Intent(context, MainService::class.java))
            } else {
                context.startService(Intent(context, MainService::class.java))
            }
        }
    }

    companion object {
        val NOTIFICATION_CHANNEL = "notificationChannel"
    }

}