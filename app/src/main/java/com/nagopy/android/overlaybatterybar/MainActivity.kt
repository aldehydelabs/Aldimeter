package com.nagopy.android.overlaybatterybar

import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.adapters.SeekBarBindingAdapter
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.ContextCompat
import android.support.v7.widget.TooltipCompat
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.CompoundButton
import android.widget.SeekBar
import android.widget.TextView
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity
import com.github.salomonbrys.kodein.instance
import com.nagopy.android.overlaybatterybar.databinding.ActivityMainBinding
import com.nagopy.android.overlayviewmanager.OverlayViewManager
import timber.log.Timber


class MainActivity : KodeinAppCompatActivity(), SeekBarBindingAdapter.OnProgressChanged {

    val overlayViewManager: OverlayViewManager by instance()
    val userSettings: UserSettings by instance()
    val serviceHandler: MainService.Handler by instance()

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.canDrawOverlays = overlayViewManager.canDrawOverlays()
        binding.onProgressChanged = this
        binding.isBatteryBarEnabled = userSettings.isBatteryBarEnabled()
        binding.isHideFullscreen = userSettings.isHideFullscreen()
        binding.isChargeAnim = userSettings.isChargeAnim()
        binding.isAdvancedStats = userSettings.isAdvancedStats()
        binding.batteryBarWidth = userSettings.getBatteryBarWidth()
        binding.batteryBarPadding = userSettings.getBatteryBarPadding();
        binding.percentageIcon = userSettings.isPercentageIcon()

        binding.changeColorsDynamically = userSettings.getChangeColorsDynamically();

        // Init index before using it
        binding.batteryBarColorIndex = 0;
        binding.toggleColorPicker = false;
        binding.batteryBarColorR = userSettings.getBatteryBarColorR(binding.batteryBarColorIndex)
        binding.batteryBarColorG = userSettings.getBatteryBarColorG(binding.batteryBarColorIndex)
        binding.batteryBarColorB = userSettings.getBatteryBarColorB(binding.batteryBarColorIndex)
        binding.batteryBarColorA = userSettings.getBatteryBarColorA(binding.batteryBarColorIndex)
        binding.batteryBarAlignment = userSettings.getBatteryBarAlignment()
        binding.showOnStatusBar = userSettings.showOnStatusBar()

        updatePreview()

        updateFab()

        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        binding.statusBarHeight =
                if (resourceId > 0) {
                    resources.getDimensionPixelSize(resourceId)
                } else {
                    32
                }

        serviceHandler.startService()

        if (savedInstanceState == null) {
            if (!overlayViewManager.canDrawOverlays()) {
                overlayViewManager.showPermissionRequestDialog(supportFragmentManager, R.string.app_name);
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Timber.d("canDrawOverlays:%s", overlayViewManager.canDrawOverlays())
        binding.canDrawOverlays = overlayViewManager.canDrawOverlays()
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.button_request_overlay_permisson -> overlayViewManager.requestOverlayPermission()
            R.id.checkbox_show_on_status_bar -> switchShowOnStatusBar((view as CompoundButton).isChecked)
            R.id.checkbox_hide_fullscreen -> switchHideFullscreen((view as CompoundButton).isChecked)
            R.id.checkbox_advanced_stats -> switchStats((view as CompoundButton).isChecked)
            R.id.checkbox_charge_anim -> switchChargeAnim((view as CompoundButton).isChecked)
            R.id.checkbox_percentage_icon -> switchPercentageIcon((view as CompoundButton).isChecked)
            R.id.fab_service_toggle -> switchBatteryBar(!binding.isBatteryBarEnabled)
            R.id.radioLeft -> switchBatteryBarAlignment(-1)
            R.id.radioCenter -> switchBatteryBarAlignment(0)
            R.id.radioRight -> switchBatteryBarAlignment(1)
            R.id.color_preview -> toggleColorPicker(0)
            R.id.color_preview_2 -> toggleColorPicker(1)
            R.id.color_preview_3 -> toggleColorPicker(2)
            R.id.radioSolid -> switchChangeColorsDynamically(-1)
            R.id.radioHard -> switchChangeColorsDynamically(0)
            R.id.radioSoft -> switchChangeColorsDynamically(1)
            R.id.radioGradient -> switchChangeColorsDynamically(2)
        }
    }

    fun switchBatteryBar(enabled: Boolean) {
        Timber.d("switchBatteryBar %s", enabled)
        userSettings.setBatteryBarEnabled(enabled)
        binding.isBatteryBarEnabled = enabled
        updateFab()
        serviceHandler.startService()
    }

    fun updateFab() {
        var fab = findViewById<FloatingActionButton>(R.id.fab_service_toggle)
        if(binding.isBatteryBarEnabled) {
            fab.setImageDrawable(ContextCompat.getDrawable(this, android.R.drawable.ic_media_pause))
            TooltipCompat.setTooltipText(fab, getText(R.string.switch_battery_bar_on))
        }
        else {
            fab.setImageDrawable(ContextCompat.getDrawable(this, android.R.drawable.ic_media_play))
            TooltipCompat.setTooltipText(fab, getText(R.string.switch_battery_bar_off))
        }
    }

    fun switchShowOnStatusBar(enabled: Boolean) {
        Timber.d("switchShowOnStatusBar %s", enabled)
        userSettings.setShowOnStatusBar(enabled)
        binding.showOnStatusBar = enabled
        serviceHandler.startService()
    }

    fun switchHideFullscreen(enabled: Boolean) {
        Timber.d("switchHideFullscreen %s", enabled)
        userSettings.setHideFullscreen(enabled)
        binding.isHideFullscreen = enabled
        serviceHandler.startService()
    }

    fun switchChargeAnim(enabled: Boolean) {
        Timber.d("switchChargeAnim %s", enabled)
        userSettings.setChargeAnim(enabled)
        binding.isChargeAnim = enabled
        serviceHandler.startService()
    }

    fun switchPercentageIcon(enabled: Boolean) {
        Timber.d("switchChargeAnim %s", enabled)
        userSettings.setPercentageIcon(enabled)
        binding.percentageIcon = enabled
        serviceHandler.startService()
    }

    fun switchStats(enabled: Boolean) {
        Timber.d("switchStats %s", enabled)
        userSettings.setAdvancedStats(enabled)
        binding.isAdvancedStats = enabled
        serviceHandler.startService()
    }

    fun switchBatteryBarAlignment(alignment: Int) {
        Timber.d("switchBatteryBarAlignment %s", alignment)
        userSettings.setBatteryBarAlignment(alignment)
        binding.batteryBarAlignment = alignment

        serviceHandler.startService()
    }

    fun switchChangeColorsDynamically(mode: Int) {
        Timber.d("switchChangeColorsDynamically %s", mode)
        userSettings.setChangeColorsDynamically(mode)
        binding.changeColorsDynamically = mode
        updatePreview()

        serviceHandler.startService()
    }

    private fun toggleColorPicker(index: Int = 0)
    {
        if (index < 0)
            return

        if (binding.batteryBarColorIndex == index && binding.toggleColorPicker)
            binding.toggleColorPicker = !binding.toggleColorPicker
        else
            binding.toggleColorPicker = true

        binding.batteryBarColorIndex = index
        updatePreview()
    }

    fun updatePreview()
    {
        if (binding.changeColorsDynamically == -1)
            binding.batteryBarColorIndex = 0

        binding.batteryBarColorR = userSettings.getBatteryBarColorR(binding.batteryBarColorIndex)
        binding.batteryBarColorG = userSettings.getBatteryBarColorG(binding.batteryBarColorIndex)
        binding.batteryBarColorB = userSettings.getBatteryBarColorB(binding.batteryBarColorIndex)
        binding.batteryBarColorA = userSettings.getBatteryBarColorA(binding.batteryBarColorIndex)

        if (binding.toggleColorPicker)
            findViewById<View>(R.id.colorPickerContainer).visibility = View.VISIBLE;
        else
            findViewById<View>(R.id.colorPickerContainer).visibility = View.GONE;

        for (i in 0 until 3) {
            // Draw a border around the selected color for contrast
            val border = GradientDrawable()
            border.setColor(Color.argb(userSettings.getBatteryBarColorA(i), userSettings.getBatteryBarColorR(i), userSettings.getBatteryBarColorG(i), userSettings.getBatteryBarColorB(i)))
            val dm = resources.displayMetrics
            val strokeWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, if (binding.batteryBarColorIndex == i && binding.toggleColorPicker) 3.0f else 1.0f, dm).toInt()
            border.setStroke(strokeWidth, resources.getColor(R.color.colorPrimary)) //black border with full opacity
            val colorView = findViewById<View>(when (i) {1 -> R.id.color_preview_2 2 -> R.id.color_preview_3 else -> R.id.color_preview})
            val subTitle = findViewById<TextView>(when (i) {1 -> R.id.preview_subtitle_2 2 -> R.id.preview_subtitle_3 else -> R.id.preview_subtitle})
            colorView.setBackgroundDrawable(border)
            colorView.visibility = if (binding.changeColorsDynamically == -1 && i > 0) View.GONE else View.VISIBLE
            subTitle.visibility = if (binding.changeColorsDynamically == -1 && i > 0) View.GONE else View.VISIBLE;
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        when (seekBar?.id) {
            R.id.seekbar_battery_bar_width -> {
                userSettings.setBatteryBarWidth(progress)
                binding.batteryBarWidth = progress
                serviceHandler.startService()
            }
            R.id.seekbar_battery_bar_padding -> {
                userSettings.setBatteryBarPadding(progress)
                binding.batteryBarPadding = progress
                serviceHandler.startService()
            }
            R.id.seekbar_battery_bar_color_R -> {
                userSettings.setBatteryBarColorR(progress, binding.batteryBarColorIndex)
                binding.batteryBarColorR = progress
                updatePreview()
                serviceHandler.startService()
            }
            R.id.seekbar_battery_bar_color_B -> {
                userSettings.setBatteryBarColorB(progress, binding.batteryBarColorIndex)
                binding.batteryBarColorB = progress
                updatePreview()
                serviceHandler.startService()
            }
            R.id.seekbar_battery_bar_color_G -> {
                userSettings.setBatteryBarColorG(progress, binding.batteryBarColorIndex)
                binding.batteryBarColorG = progress
                updatePreview()
                serviceHandler.startService()
            }
            R.id.seekbar_battery_bar_color_A -> {
                userSettings.setBatteryBarColorA(progress, binding.batteryBarColorIndex)
                binding.batteryBarColorA = progress
                updatePreview()
                serviceHandler.startService()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_license -> {
                startActivity(Intent(this, LicenseActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
