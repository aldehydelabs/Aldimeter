package com.nagopy.android.overlaybatterybar

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.animation.ValueAnimator.INFINITE
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.BatteryManager
import android.os.Build
import android.os.PowerManager
import android.provider.Settings
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.animation.AccelerateDecelerateInterpolator
import com.nagopy.android.overlayviewmanager.OverlayViewManager
import timber.log.Timber


@SuppressLint("ALLOW_VIEW_TO_EXTEND_OUTSIDE_SCREEN")
class BatteryBarDelegate(
        val context: Context
        , val powerManager: PowerManager
        , val overlayViewManager: OverlayViewManager
        , val userSettings: UserSettings) {

    val barView = overlayViewManager.newOverlayView(View(context).apply {
        setBackgroundColor(Color.WHITE)
        layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
    }).apply {
        setHeight(6)
        allowViewToExtendOutsideScreen(true)
        setAlpha(0.8f)
    }

    val intentFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED).apply {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            addAction(PowerManager.ACTION_POWER_SAVE_MODE_CHANGED)
        }
    }

    var batteryLevel: Int = 0
    var batteryScale: Int = 0
    var isStarted: Boolean = false
    var chargeAnim: ObjectAnimator? = null;
    var activeAlignment: Int = -2

    fun start() {
        Timber.d("startService isStarted:%s", isStarted)
        if (!isStarted) {
            Timber.d("start")
            context.registerReceiver(receiver, intentFilter)
            barView.apply {

                setHeight(userSettings.getBatteryBarWidth())
            }.show()

            updateBarColors()
        }
        isStarted = true
    }

    fun stop() {
        Timber.d("stopService isStarted:%s", isStarted)
        if (isStarted) {
            Timber.d("stop")
            context.unregisterReceiver(receiver)
            barView.hide()
        }
        isStarted = false
    }

    fun isEnabled(): Boolean = userSettings.isBatteryBarEnabled()


    private fun interpolate(a: Float, b: Float, proportion: Float): Float {
        return a + (b - a) * proportion
    }

    /**
     * Returns an interpolated color, between `a` and `b`
     * proportion = 0, results in color a
     * proportion = 1, results in color b
     */
    private fun interpolateColor(a: Int, b: Int, proportion: Float): Int {
        if (proportion > 1 || proportion < 0) {
            throw IllegalArgumentException("proportion must be [0 - 1]")
        }
        val hsva = FloatArray(3)
        val hsvb = FloatArray(3)
        val hsv_output = FloatArray(3)
        Color.colorToHSV(a, hsva)
        Color.colorToHSV(b, hsvb)
        for (i in 0..2) {
            hsv_output[i] = interpolate(hsva[i], hsvb[i], proportion)
        }
        val alpha_a = Color.alpha(a)
        val alpha_b = Color.alpha(b)
        val alpha_output = interpolate(alpha_a.toFloat(), alpha_b.toFloat(), proportion)
        return Color.HSVToColor(alpha_output.toInt(), hsv_output)
    }

    private fun updateBarColors()
    {
        var color0 = Color.argb(userSettings.getBatteryBarColorA(), userSettings.getBatteryBarColorR(), userSettings.getBatteryBarColorG(), userSettings.getBatteryBarColorB())
        var color1 = Color.argb(userSettings.getBatteryBarColorA(1), userSettings.getBatteryBarColorR(1), userSettings.getBatteryBarColorG(1), userSettings.getBatteryBarColorB(1))
        var color2 = Color.argb(userSettings.getBatteryBarColorA(2), userSettings.getBatteryBarColorR(2), userSettings.getBatteryBarColorG(2), userSettings.getBatteryBarColorB(2))

        // TODO: Don't assume a scale from 0 to 100
        when (userSettings.getChangeColorsDynamically())
        {
            -1 -> {
                barView.view.setBackgroundColor(color0)
            }
            0 -> {
                if (batteryLevel >= 50)
                    barView.view.setBackgroundColor(color0)
                else if (batteryLevel >= 15)
                    barView.view.setBackgroundColor(color1)
                else
                    barView.view.setBackgroundColor(color2)
            }
            1 -> {
                if (batteryLevel >= 50)
                    barView.view.setBackgroundColor(interpolateColor(color1, color0, (batteryLevel - 50).toFloat() / 50.0f))
                else
                    barView.view.setBackgroundColor(interpolateColor(color2, color1, batteryLevel.toFloat() / 50))
            }
            2 -> {

                var colorArray  = intArrayOf()

                if (batteryLevel >= 50)
                    colorArray = intArrayOf(color2, color1, interpolateColor(color1, color0, (batteryLevel - 50).toFloat() / 50.0f))
                else
                    colorArray = intArrayOf(color2, interpolateColor(color2, color1, batteryLevel.toFloat() / 50), interpolateColor(color2, color1, batteryLevel.toFloat() / 50.toFloat()))

                if (colorArray.size < 2)
                    barView.view.setBackgroundColor(color2)
                else
                {
                    val gradientDrawable = GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT,
                            colorArray
                    );
                    gradientDrawable.cornerRadius = 0f;

                    barView.view.background = gradientDrawable
                }
            }
        }
    }

    fun updateBatteryLevel(level: Int, scale: Int, isCharging: Boolean?) {
        batteryLevel = level
        batteryScale = scale
        // Calculate the padding in pixel based on percentage
        var padding = ((overlayViewManager.displayWidth / 2) * (userSettings.getBatteryBarPadding()/100f)).toInt()
        val newWidth = (overlayViewManager.displayWidth - (padding * 2)) * level / scale.toFloat()
        barView.apply {

            updateBarColors()

            when (userSettings.getBatteryBarAlignment()) {
                -1 -> {
                    setX(padding)
                }
                0 -> {
                    var diff = (overlayViewManager.displayWidth - (padding * 2)) - newWidth
                    if (!isCharging!! || !userSettings.isChargeAnim())
                        setX(padding + (diff / 2).toInt())
                    else
                        setX(padding)

                    if (isCharging && userSettings.isChargeAnim())
                        view.pivotX = (overlayViewManager.displayWidth / 2f) - padding
                    else
                        view.pivotX = (newWidth / 2)
                }
                1 -> {
                    if (!isCharging!! || !userSettings.isChargeAnim())
                        setX((overlayViewManager.displayWidth - newWidth - padding).toInt());
                    else
                        setX(padding)
                }
            }

            setWidth(newWidth.toInt())

            // In case we are charging rn, make the bar as wide as the screen so the animation works
            if (isCharging != null) {
                if (isCharging!! && userSettings.isChargeAnim()) {
                    setWidth(overlayViewManager.displayWidth - (padding * 2))
                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                allowViewToExtendOutsideScreen(userSettings.showOnStatusBar() && !powerManager.isPowerSaveMode)
            } else {
                allowViewToExtendOutsideScreen(userSettings.showOnStatusBar())
            }
        }
        Timber.d("level = %d, scale = %d, width = %f", level, scale, newWidth)

        barView.update()


        // Charging animator
        if (isCharging != null) {
            if (isCharging!!  && userSettings.isChargeAnim()) {

                // Alignment has been changed, end animation and null it so we can set a new one
                if (userSettings.getBatteryBarAlignment() != activeAlignment) {
                    if (chargeAnim != null) {

                        // Prevent scaling to be dangling in mid-air, else the animation stops where it shouldn't
                        if (userSettings.getBatteryBarAlignment() == 0)
                            chargeAnim!!.setFloatValues(0f, 1f)

                        chargeAnim!!.end()
                        chargeAnim = null;
                    }

                    activeAlignment = userSettings.getBatteryBarAlignment()
                }

                if (chargeAnim == null) {
                    fixAnimation()
                    when (activeAlignment) {
                        -1, 1 -> {
                            chargeAnim = ObjectAnimator.ofFloat(barView.view, View.TRANSLATION_X, -(overlayViewManager.displayWidth - (padding * 2) - newWidth.toInt()).toFloat(), (overlayViewManager.displayWidth - (padding * 2) - newWidth.toInt()).toFloat()).apply {
                                duration = 8000 - (6000 * level / scale.toFloat()).toLong()
                                interpolator = AccelerateDecelerateInterpolator()
                                repeatCount = INFINITE
                                repeatMode = ValueAnimator.REVERSE

                                if (activeAlignment == -1)
                                    start()
                                else
                                    reverse()

                            }
                        }
                        0 -> {
                            chargeAnim = ObjectAnimator.ofFloat(barView.view, View.SCALE_X, level / scale.toFloat(),1f).apply {
                                duration = 5000 - (4000 * level / scale.toFloat()).toLong()
                                interpolator = AccelerateDecelerateInterpolator()
                                repeatCount = INFINITE
                                repeatMode = ValueAnimator.REVERSE
                                start()
                            }
                        }
                    }
                }
                else
                {
                    when (userSettings.getBatteryBarAlignment()) {
                        -1, 1 -> {
                            chargeAnim!!.setFloatValues(-(overlayViewManager.displayWidth - (padding * 2) - newWidth.toInt()).toFloat(), (overlayViewManager.displayWidth - (padding * 2) - newWidth.toInt()).toFloat());
                            chargeAnim!!.duration = 8000 - (6000 * level / scale.toFloat()).toLong()
                        }
                        0 -> {
                            chargeAnim!!.setFloatValues(level / scale.toFloat(),1f);
                            chargeAnim!!.duration = 5000 - (4000 * level / scale.toFloat()).toLong();
                        }
                    }
                }
            } else {
                if (chargeAnim != null) {
                    fixAnimation()
                    chargeAnim!!.end()
                    chargeAnim!!.duration = 2500

                    when (userSettings.getBatteryBarAlignment()) {
                        -1 -> {
                            chargeAnim!!.setFloatValues(-(newWidth).toFloat(), 0f);
                            chargeAnim!!.repeatCount = 0
                            chargeAnim!!.start()
                        }
                        0 -> {
                            chargeAnim!!.setFloatValues(0f, 1f)
                            chargeAnim!!.repeatCount = 0
                            chargeAnim!!.start()
                        }
                        1 -> {
                            chargeAnim!!.setFloatValues(0f, newWidth.toFloat())
                            chargeAnim!!.repeatCount = 0
                            chargeAnim!!.reverse()
                        }
                    }

                    chargeAnim = null;
                }
            }
        }
    }

    private fun fixAnimation() {
        val durationScale = Settings.Global.getFloat(this.context.contentResolver, Settings.Global.ANIMATOR_DURATION_SCALE, 1f)
        Timber.d("fixAnimation", "durationScale: $durationScale")
        if (durationScale != 1f) {
            try {
                ValueAnimator::class.java.getMethod("setDurationScale",Float::class.javaPrimitiveType).invoke(null, 1f)
            } catch (t: Throwable) {
                Timber.e("fixAnimation", t.message ?: t.toString())
            }
        }
    }


    val receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Intent.ACTION_BATTERY_CHANGED -> {
                    val level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
                    val scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)

                    val status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1)
                    val temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1)
                    val voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1)
                    val isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL

                    updateBatteryLevel(level, scale, isCharging)

                    batteryChangedCallback?.invoke(level, scale, isCharging, temperature, voltage)
                }
                PowerManager.ACTION_POWER_SAVE_MODE_CHANGED -> {
                    updateBatteryLevel(batteryLevel, batteryScale, null)
                }
            }

        }
    }

    var batteryChangedCallback: ((level: Int, scale: Int, isCharging: Boolean, temperature: Int, voltage: Int) -> Unit)? = null

}
