package com.nagopy.android.overlaybatterybar

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.webkit.WebView
import android.widget.Toast
import java.nio.charset.Charset
import java.util.*

class LicenseActivity : AppCompatActivity() {

    lateinit var webView: WebView
    var randomizer = Random(System.currentTimeMillis())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        webView = WebView(this)
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.loadUrl("file:///android_asset/licenses.html")
        setContentView(webView)


        if (randomizer.nextInt(5) == 3)
            Toast.makeText(this, String(Base64.getDecoder().decode(getText(R.string.easter_egg).toString()), Charset.forName("UTF-8")), Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        webView.destroy()
        super.onDestroy()
    }
}