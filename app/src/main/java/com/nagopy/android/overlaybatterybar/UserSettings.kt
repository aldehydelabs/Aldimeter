package com.nagopy.android.overlaybatterybar

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Build

class UserSettings(val sharedPreferences: SharedPreferences) {

    fun isBatteryBarEnabled(): Boolean
            = sharedPreferences.getBoolean("isBatteryBarEnabled", false)

    fun isHideFullscreen(): Boolean
            = sharedPreferences.getBoolean("hideFullscreen", false)

    fun isChargeAnim(): Boolean
            = sharedPreferences.getBoolean("chargeAnim", Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)

    fun isAdvancedStats(): Boolean
            = sharedPreferences.getBoolean("advancedStats", false)

    fun isPercentageIcon(): Boolean
            = sharedPreferences.getBoolean("percentageIcon", false)

    @SuppressLint("ApplySharedPref")
    fun setBatteryBarEnabled(enabled: Boolean) {
        sharedPreferences.edit().putBoolean("isBatteryBarEnabled", enabled).commit()
    }

    fun getBatteryBarWidth(): Int
            = sharedPreferences.getInt("batteryBarWidth", 6)

    fun getBatteryBarPadding(): Int
            = sharedPreferences.getInt("batteryBarPadding", 0)

    fun getBatteryBarColorR(index: Int = 0): Int
            = sharedPreferences.getInt("batteryBarColorR" + if (index > 0) index.toString() else "", when (index) {1 -> 253 2 -> 229 else -> 205} )

    fun getBatteryBarColorG(index: Int = 0): Int
            = sharedPreferences.getInt("batteryBarColorG" + if (index > 0) index.toString() else "", when (index) {1 -> 216 2 -> 57 else -> 220} )

    fun getBatteryBarColorB(index: Int = 0): Int
            = sharedPreferences.getInt("batteryBarColorB" + if (index > 0) index.toString() else "", when (index) {1 -> 53 2 -> 53 else -> 57} )

    fun getBatteryBarColorA(index: Int = 0): Int
            = sharedPreferences.getInt("batteryBarColorA" + if (index > 0) index.toString() else "", 180)

    fun getBatteryBarAlignment(): Int
            = sharedPreferences.getInt("batteryBarAlignment", -1)

    fun setHideFullscreen(hide: Boolean) {
        sharedPreferences.edit().putBoolean("hideFullscreen", hide).commit()
    }

    fun setChargeAnim(anim: Boolean) {
        sharedPreferences.edit().putBoolean("chargeAnim", anim).commit()
    }

    fun setAdvancedStats(stats: Boolean) {
        sharedPreferences.edit().putBoolean("advancedStats", stats).commit()
    }

    fun setPercentageIcon(enabled: Boolean) {
        sharedPreferences.edit().putBoolean("percentageIcon", enabled).commit()
    }

    @SuppressLint("ApplySharedPref")
    fun setBatteryBarWidth(newWidth: Int) {
        sharedPreferences.edit().putInt("batteryBarWidth", newWidth).commit()
    }

    fun setBatteryBarPadding(newPadding: Int) {
        sharedPreferences.edit().putInt("batteryBarPadding", newPadding).commit()
    }

    fun setBatteryBarColorR(newR: Int, index: Int = 0) {
        sharedPreferences.edit().putInt("batteryBarColorR" + if (index > 0) index.toString() else "", newR).commit()
    }

    fun setBatteryBarColorG(newG: Int, index: Int = 0) {
        sharedPreferences.edit().putInt("batteryBarColorG" + if (index > 0) index.toString() else "", newG).commit()
    }

    fun setBatteryBarColorB(newB: Int, index: Int = 0) {
        sharedPreferences.edit().putInt("batteryBarColorB" + if (index > 0) index.toString() else "", newB).commit()
    }

    fun setBatteryBarColorA(newA: Int, index: Int = 0) {
        sharedPreferences.edit().putInt("batteryBarColorA" + if (index > 0) index.toString() else "", newA).commit()
    }

    fun setBatteryBarAlignment(alignment: Int) {
        sharedPreferences.edit().putInt("batteryBarAlignment", alignment).commit()
    }


    fun showOnStatusBar(): Boolean
            = sharedPreferences.getBoolean("showOnStatusBar", true)

    @SuppressLint("ApplySharedPref")
    fun setShowOnStatusBar(enabled: Boolean) {
        sharedPreferences.edit().putBoolean("showOnStatusBar", enabled).commit()
    }

    fun getChangeColorsDynamically(): Int = sharedPreferences.getInt("changeColorsDynamically", -1)

    fun setChangeColorsDynamically(changeColors: Int) {
        sharedPreferences.edit().putInt("changeColorsDynamically", changeColors).commit()
    }
}